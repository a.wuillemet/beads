/// <reference path="util.ts" />
/// <reference path="eel.ts" />

interface Defaults {
    [key: string]: {
        'typevalue': any,
        'current': any,
        'description': string
    }
}

abstract class OPT<TYPE> extends ElementWrapper {
    protected content: HTMLElement;

    constructor(public title: string, value: TYPE, description: string) {
        super(document.createElement('div'));
        this.setContent(value);
        this.element.appendChild(this.content);
        let label = this.create('span', {"content": title});
        this.element.appendChild(label);
        let help = this.create('p', {"content": description});
        this.element.appendChild(help);
    }

    abstract setContent(value: TYPE): void;

    abstract getValue(): string;
}

class StringOption extends OPT<string> {

    constructor(title: string, value: string, description: string) {
        super(title, value, description);
    }
    
    public getValue(): string {
        return this.content['value'];
    }
    
    setContent(value: string): void {
        if(this.content) {
            this.element.removeChild(this.content);
        }
        this.content = this.create('input', {'type': 'text', "value": value});
        this.element.insertBefore(this.content, this.element.firstChild);
    }

}

class ToggleOption extends OPT<boolean>{

    private selected: boolean;

    constructor(title: string, value: boolean, description: string) {
        super(title, value, description);
        this.element.classList.add('toggle');
    }

    public getValue(): string {
        return `${this.content.classList.contains('selected')}`;
    }

    setContent(value: boolean): void {
        this.selected = value || false;
        let line = this.create('div', {'classList': 'line horizontal'});
        let circle = this.create('div', {'classList': 'button'});
        this.content = this.create('button', {'content': [circle, line], 'classList': this.selected ? 'selected' : ''});
        this.content.addEventListener('click', () => this.handleClick());
    }

    private handleClick(): void {
        if(this.selected) {
            this.content.classList.remove('selected');
            this.selected = false;
        } else {
            this.content.classList.add('selected');
            this.selected = true;
        }
    }
}

class NumberOption extends OPT<number> {

    constructor(title: string, value: number, description: string) {
        super(title, value, description);
    }
    
    public getValue(): string {
        return this.content["value"];
    }
    
    setContent(value: number): void {
        let strValue = value === -1 ? '' : value.toString();
        this.content = this.create('input', {'type': 'text', "value": strValue});
        this.content.addEventListener('input', this.correctInput, false);
    }

    private correctInput(): void {
        this.content['value'] = this.content['value'].replace(/[^0-9]/g, '');
    }
}

class BackendOptions extends ElementWrapper {
    
    private options: OPT<any>[];

    constructor(element: HTMLElement) {
        super();
        this.options = [];
        this.element = this.create('div');
        this.appendSelf(element);
    }

    public createOption(title: string, type: string, value: any, description: string) {
        let option: OPT<any>;

        if (type === 'boolean') {
            value = value || false;
            option = new ToggleOption(title, value, description);      
            
        } else if (type === 'string') {
            value = value || '';
            option = new StringOption(title, value, description);
            
        } else if (type === 'number') {
            value = value || -1;
            option = new NumberOption(title, value, description);

        }

        if(option) {
            this.options.push(option);
            option.appendSelf(this.element);
        }
    }

    public getGenerationOptions(): Options {
        for (const option of this.options) {
            if(option.title === 'skip-validation') {
                return {'skip-validation': option.getValue()};
            }
        }
    }

    public getSelectedLanguage(): string {
        for (const option of this.options) {
            if(option.title === 'language') {
                return option.getValue();
            }
        }
    }

    public setSelectedLanguage(language: string): void {
        for (const option of this.options) {
            if(option.title === 'language') {
                option.setContent(language);
            }
        }
    }

    private setup(defaults: Defaults) {
        Object.keys(defaults).forEach(key => {
            let opt = defaults[key];
            let type = typeof(opt['typevalue']);
            let value = opt['current'];
            let desc = opt['description'];
            this.createOption(key, type, value, desc);
        });
    }

    public init(): void {
        EEL.availableDefaults(defaults => this.setup(defaults));
    }

    public save(): void {
        this.options.forEach(option => {
            let valueToSet = option.getValue();
            if(valueToSet) {
                EEL.setOption(option.title, valueToSet);
            } else {
                EEL.unsetOption(option.title);
            }
        });
    }
}

const BACKENDOPTIONS = new BackendOptions(document.getElementById('options'));
BACKENDOPTIONS.init();
