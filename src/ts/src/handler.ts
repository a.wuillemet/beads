/// <reference path="options.ts" />
/// <reference path="eel.ts" />
/// <reference path="prism.d.ts" />

let lastGeneratedCode: [string, string];

d3.select("#code-gen-button").on("click", () => {
    let fsm = {
        'name': graph.name,
        "nodes": graph.nodes,
        "transitions": graph.transitions
    };

    let selectedLanguage: string = BACKENDOPTIONS.getSelectedLanguage();
    let options: Options = BACKENDOPTIONS.getGenerationOptions();

    EEL.parse(fsm, selectedLanguage, options, (code, error) => displayCode(code, error, selectedLanguage));
});

d3.select("#toggle-code-button").on("click", () => {
    let btn = d3.select("#toggle-code-button");
    let select = !btn.classed('selected');

    btn.classed('selected', select);
    d3.select("#text-area").classed('selected', select);
    d3.select("#copy-code-btn").classed("hidden", !select);
});

d3.select("#delete-btn").on("click", () => {
   d3.select("#code-container").html("");
   d3.select("#toggle-code-button").classed("error", false);
   graph.transitions = [];
   graph.nodes = [];
   i = 0;
   j = 0;
   render()
});

d3.select('#save-options-btn').on('click', () => BACKENDOPTIONS.save());

d3.select('#clear-options-btn').on('click', () => EEL.unsetAllOptions());

d3.select('#save-as-svg-btn').on('click', () => saveSVG());

d3.select('#save-as-png-btn').on('click', () => savePNG());

d3.select('#save-as-json-btn').on('click', () => saveJson());

d3.select('#save-code-btn').on('click', () => saveLastGeneratedCode());

d3.select('#copy-code-btn').on('click', () => copyCode());

let nameField = d3.select('#graph-name-field')
    .on('focusout', () => changedGraphName())
    .on("keypress", () => {
        if(d3.event.keyCode == 13) {
            changedGraphName();
            let input = nameField.node() as HTMLInputElement;
            input.blur();
        }
    })
    .on("focus", () => {
        let input = nameField.node() as HTMLInputElement;
        input.select()
    });

function changedGraphName(): void {
    let newName = changedLabel(nameField, d3.event);
    nameField.property("value", newName);
    graph.name = newName;
}

function saveLastGeneratedCode(): void {
    if(lastGeneratedCode) {
        saveCode(lastGeneratedCode[0], lastGeneratedCode[1]);
    } else {
        window.alert('No valid code can be saved!');
    }
}

function displayCode(code: string, error: string, language: string): void {
    if(error) {
        lastGeneratedCode = undefined;

        d3.select("#code-container")
            .html(`ERROR:\n\n${error}`)
            .classed("error", true)
        d3.select('#toggle-code-button')
            .classed("error", true);
    } else {
        lastGeneratedCode = [code, language];

        let codeContainer = d3.select("#code-container");
        if(d3.select('#text-area').classed('selected')) {
            codeContainer
                .html(`<pre><code class="language-${language}">${code}</code></pre>`)
                .classed("error", false);
            d3.select("#toggle-code-button")
                .classed("error", false);
            Prism.highlightAllUnder(document.getElementById("code-container"));
        } else {
            saveCode(code, language);
        }
    }
}

function setVersion(version: string): void {
    d3.select("#version-span").html("Version: " + version);
}

function copyCode(): void {
    let code = lastGeneratedCode[0];

    let cache = document.createElement("textarea");
    document.body.appendChild(cache);

    try {
        cache.value = code;
        cache.focus();
        cache.select();
        document.execCommand('copy');
        console.log(`Copied to clipboard: \n${cache.value}`);

    } catch (error) {
        console.log(`Copy failed. ${error}`);

    } finally {
        document.body.removeChild(cache);
    }
}

EEL.getVersion(setVersion);
