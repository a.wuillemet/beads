/// <reference path="util.ts"/>
/// <reference path="prism.d.ts" />

const previewGraph: Schema = {
    'name': 'PreviewGraph',
    'nodes': [
        {id: "Initial", start: true},
        {id: "Intermediate"},
        {id: "End"}
    ],
    'transitions': [
        {label: "step", from: "Initial", to: "Intermediate", visible: true, offset: null},
        {label: "step", from: "Intermediate", to: "End", visible: true, offset: null},
        {label: "back", from: "End", to: "Intermediate", visible: true, offset: null},
        {label: "back", from: "Intermediate", to: "Initial", visible: true, offset: null},
        {label: "finish", from: "Initial", to: "End", visible: true, offset: null}
    ]
}

/**
 * Class representing a language.
 * It is used to generate a Preview for the code generated with this language template.
 */
class LanguagePreview extends ElementWrapper {

    private static IMG_SRC = "media/language-graph.svg";
    private static OPTS = {"skip-validation": true};

    constructor(public language: string) {
        super();
        let graph = this.create('img', {'src': LanguagePreview.IMG_SRC});
        let innerDiv = this.create('div', {'content': [graph]});
        let pre = this.create('pre', {'classList': `language-${language}`});
        this.element = this.create('div', {'content': [innerDiv, pre]});

        EEL.parse(previewGraph, this.language, LanguagePreview.OPTS, (code, error) => {
            if(error) {
                console.log(error);
                pre.innerHTML = "No code preview available!"
            } else {
                pre.appendChild(this.create('code', {'content': code}));
                Prism.highlightAllUnder(pre);
            }
        });
    }
}

/**
 * Wrapper for all language previews.
 */
class LanguageHelp extends ElementWrapper {

    private selection: SelectionHandler;

    constructor(private container: HTMLElement) {
        super(document.createElement('ul'));
        let creator = (language: string) => {
            let option = this.create('li', {"id": language, "content": language});
            option.addEventListener("click", () => BACKENDOPTIONS.setSelectedLanguage(language));
            return option;
        };
        this.selection = new SelectionHandler(this.element, creator);
        this.selection.appendSelf(this.container);
    }

    public appendPreview(preview: LanguagePreview): void {
        preview.appendSelf(this.container);
        this.selection.createOption(preview.language, [preview.element]);
    }

    public init(): void {
        EEL.availableLanguages(languages => {
            languages.forEach(language => {
                this.appendPreview(new LanguagePreview(language));
            });
        });
    }
}

const LANGUAGES = new LanguageHelp(document.getElementById('languages'));
LANGUAGES.init();
