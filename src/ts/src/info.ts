function setInfo(info: string) {
    info = info.replace(/\\n|"/g, "")
            .replace(/\\u00a9/g, '&copy;')
            .replace(/<p><img(.*?)<\/p>/g, "");

    let article = d3.create("article")
        .html(info)
        .node();

    // Clear shell info code
    let shell = "\\shell\\";
    let languageshell = "language-shell";
    article.querySelectorAll(`.\\${shell}\\`).forEach(code => {
        code.classList.remove(shell);
        code.classList.add(languageshell);
        code.innerHTML = code.innerHTML.replace(/ /g, "  ")
            .replace(/        -/g, "\n\n    -")
            .replace(/#/g, "\n#")
            .replace(/\$/g, "\n\n$");
    });

    // Clear json info code
    let json = "\\json\\";
    let languagejson = "language-json";
    article.querySelectorAll(`.\\${json}\\`).forEach(code => {
        code.classList.remove(json);
        code.classList.add(languagejson);
        code.innerHTML = code.innerHTML.replace("{", "\n{")
            .replace(/\s\s+{/g, "\n   {")
            .replace(/\s\s+"/g, '\n"')
            .replace(/\s\s+]/g, "\n]");
    });

    // Clear .bead info code
    let codes = article.querySelectorAll(`code`);
    let beadCode = codes[codes.length - 1]
    beadCode.innerHTML = beadCode.innerHTML.replace(/\s\s+/g, "\n")
        .replace("#!", "\n#!");

    article.querySelectorAll("p a").forEach(a => {
        let link = a as HTMLLinkElement;
        link.innerHTML = link.href.replace(/http:\/\/localhost:[0-9]*\//g, "");
    });

    let logo = d3.create("img")
        .attr("src", "media/logo.png")
        .node();
    article.insertBefore(logo, article.firstChild);

    Prism.highlightAllUnder(article);
    document.getElementById("info").appendChild(article);
}

EEL.getInfo(setInfo);