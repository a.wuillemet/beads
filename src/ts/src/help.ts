/**
 * Class to setup navigation of tutorial pages.
 */
class TutorialHelp extends Creator {

    private selection: SelectionHandler;

    constructor() {
        super();
        this.selection = new SelectionHandler();
    }

    public init(): void {
        let items: NodeListOf<HTMLElement> = document.querySelectorAll('.help-item');

        for (let index = 0; index < items.length; index++) {
            const item: HTMLElement = items[index];

            const previousBtn = this.createButton("previous");
            const nextBtn = this.createButton("next");
            const div = this.create('div', {'classList': 'navigate', "content": [previousBtn, nextBtn]});
            
            if (index > 0) {
                this.selection.addOption(previousBtn, [items[index - 1]]);
            } else {
                previousBtn.classList.add('disabled');
            }
            
            if (index < items.length - 1) {
                this.selection.addOption(nextBtn, [items[index + 1]]);
            } else {
                nextBtn.classList.add('disabled');
            }

            if(index === 1) {
                // Initially display first tutorial page
                this.selection.select(previousBtn);
            }

            item.appendChild(div);
        }
    }

    private createButton(s: string): HTMLElement {
        return this.create('button', {"content": s, "classList": s});
    }
}

const TUTORIAL = new TutorialHelp();
TUTORIAL.init();
