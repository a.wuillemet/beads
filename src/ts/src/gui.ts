const graph : Schema = {
   name : "unnamed_graph",
   nodes : [],
   transitions: []
}

let r = 50;
let i = 1, j = 1;
let mousedownNode = null;
let mousedownTransition = null;

let svg = d3.select("#graph-container").append("svg");

svg.on("dblclick", function () {
    if (d3.event.ctrlKey || d3.event.metaKey) {
        let [x, y] = d3.mouse(this);
        graph.nodes.push({ x: x, y: y, id: defaultStateName() });
        render();
    }
});

// creating arrows
svg.append("defs").append('marker')
    .attr('id', 'arrow')
    .attr('viewBox', '0 0 10 10')
    .attr('refX', 9)
    .attr('refY', 5)
    .attr('markerWidth', 5)
    .attr('markerHeight', 5)
    .attr('orient', 'auto') 
   .append("path")
      .attr("d", "M 0 0 L 10 5 L 0 10 z");

let arrow = svg.append('path')
    .attr('d', 'M 200,200 L 400,400')
    .attr('fill', '#000')
    .attr('stroke-width', '3px')
    .attr('stroke', '#000')
    .attr("marker-end", "url(#arrow)")
    .style("display", "none");

svg.on("mousemove", function() {
   if(mousedownNode != null) {
   const [x, y] = d3.mouse(this);
   arrow
      .attr('d', `M ${mousedownNode.x},${mousedownNode.y} L ${x},${y}`)
      .style("display", "block");
   }

   if(mousedownTransition != null) {
       const [x, y] = d3.mouse(this);
       mousedownTransition.offset = {x: x, y: y};
       console.log(mousedownTransition.offset);
   }

   if(mousedownTransition || mousedownNode) {
      render();
   }
});

svg.on('mouseup', function() {
   mousedownNode = null;
   mousedownTransition = null;
   arrow.style("display", "none");
   render();
});

function render() {
    let node = svg.selectAll(".state").data(graph.nodes);
    let group = node.enter()
        .append("g")
        .classed("state", true)
        .on("click", (state) => {
           if (d3.event.shiftKey) {
               graph.nodes.splice(graph.nodes.indexOf(state), 1);
               graph.transitions = graph.transitions.filter(t => {return t.from!=state.id && t.to!=state.id});
               render();
           }
           if(d3.event.altKey){
              if(graph.transitions.find((t) => t.from==state.id && t.to==state.id)!==undefined){return;}

              let newTransition = {
                 from: `${state.id}`,
                 to: `${state.id}`,
                 label: defaultTransitionName(),
                 visible: true,
                 offset: null
              };
              graph.transitions.push(newTransition);
              render();
           }
           
        })
         .on("mousedown", state => {
             mousedownNode = state;
         })
      .on("mouseup", state => {
         if(mousedownNode == null || mousedownNode == state){ return; }
         if(graph.transitions.find((t) => t.from==mousedownNode.id && t.to==state.id)!==undefined){return;}

         let newTransition = {
            from: `${mousedownNode.id}`,
            to: `${state.id}`,
            label: defaultTransitionName(),
            visible: true,
            offset: null
         };
         graph.transitions.push(newTransition);
         mousedownNode = null;
      });

    group.append("circle")
        .attr("r", r)
        .merge(node.select("circle"))
        .attr("cx", (state) => state.x)
        .attr("cy", (state) => state.y);

    group.append("text")
        .call(make_editable, "id")
        .attr("dominant-baseline", "middle")
        .attr("text-anchor", "middle")
        .merge(node.select("text"))
        .text((state) => state.id)
        .attr("x", (state) => state.x)
        .attr("y", (state) => state.y);

    node.exit().remove();

    let transition = svg.selectAll(".transition").data(graph.transitions);
    let tgroup = transition.enter()
        .append("g")
        .classed("transition", true)
        .on("mousedown", transition => {
            mousedownTransition = transition;
        })
        .on("click", function(d) {
           if(d3.event.shiftKey) {
            graph.transitions.splice(graph.transitions.indexOf(d), 1);
               render();
           }
        });

    tgroup.append("path")
       .attr('fill', 'none')
       .attr('stroke-width', '3px')
       .attr('stroke', '#000')
       .attr("marker-end", "url(#arrow)")
       .merge(transition.select("path"))
       .attr('d', (d : any) => {
          let from = graph.nodes.find((s) => s.id==d.from);
          let to = graph.nodes.find((s) => s.id==d.to);
          let xDiff = to.x - from.x;
          let yDiff = to.y - from.y;
          let arrowLength = Math.sqrt(xDiff*xDiff + yDiff*yDiff);
          let t = 0.5;

          if(from == to){
             d.loop = true;
             let x = r * Math.cos(-Math.PI/4);
             let y = r * Math.sin(-Math.PI/4);
            return `M ${from.x-x},${from.y+y} A 50,50 0,1,1 ${to.x+x},${to.y+y}`;
          }

          if(arrowLength < 2*r){ d.visible=false; return; }
          d.visible = true;

          // Q1
          if(to.y <= from.y && to.x >= from.x){
             let a = Math.atan((from.y-to.y) / (to.x-from.x));
             let x = r * Math.cos(a);
             let y = r * Math.sin(a);
             if(d.offset != null){
                let startX = from.x+x;
                let startY = from.y-y;
                let endX = to.x-x;
                let endY = to.y+y;
                let controlX = d.offset.x/(2*t*(1-t))-startX*t/(2*(1-t))-endX*(1-t)/(2*t);
                let controlY = d.offset.y/(2*t*(1-t))-startY*t/(2*(1-t))-endY*(1-t)/(2*t);
                return `M ${startX},${startY} Q ${controlX},${controlY} ${endX},${endY}`;
             }
             return `M ${from.x+x},${from.y-y} L ${to.x-x},${to.y+y}`;
          }
          // Q2
          if(to.y >= from.y && to.x >= from.x){
             let a = Math.atan((to.y-from.y) / (to.x-from.x));
             let x = r * Math.cos(a);
             let y = r * Math.sin(a);
               if(d.offset != null){
                   let startX = from.x+x;
                   let startY = from.y+y;
                   let endX = to.x-x;
                   let endY = to.y-y;
                   let controlX = d.offset.x/(2*t*(1-t))-startX*t/(2*(1-t))-endX*(1-t)/(2*t);
                   let controlY = d.offset.y/(2*t*(1-t))-startY*t/(2*(1-t))-endY*(1-t)/(2*t);
                   return `M ${startX},${startY} Q ${controlX},${controlY} ${endX},${endY}`;

               }
               return `M ${from.x+x},${from.y+y} L ${to.x-x},${to.y-y}`;
          }
          // Q3
          if(to.y >= from.y && to.x <= from.x){
             let a = Math.atan((to.y-from.y) / (from.x-to.x));
             let x = r * Math.cos(a);
             let y = r * Math.sin(a);
             if(d.offset != null) {
                let startX = from.x-x;
                let startY = from.y+y;
                let endX = to.x+x;
                let endY = to.y-y;
                let controlX = d.offset.x/(2*t*(1-t))-startX*t/(2*(1-t))-endX*(1-t)/(2*t);
                let controlY = d.offset.y/(2*t*(1-t))-startY*t/(2*(1-t))-endY*(1-t)/(2*t);
                return `M ${startX},${startY} Q ${controlX},${controlY} ${endX},${endY}`;
             }
             return `M ${from.x-x},${from.y+y} L ${to.x+x},${to.y-y}`;
          }
          // Q4
          if(to.y <= from.y && to.x <= from.x){
             let a = Math.atan((from.y-to.y) / (from.x-to.x));
             let x = r * Math.cos(a);
             let y = r * Math.sin(a);
             if(d.offset != null) {
                let startX = from.x-x;
                let startY = from.y-y;
                let endX = to.x+x;
                let endY = to.y+y;
                let controlX = d.offset.x/(2*t*(1-t))-startX*t/(2*(1-t))-endX*(1-t)/(2*t);
                let controlY = d.offset.y/(2*t*(1-t))-startY*t/(2*(1-t))-endY*(1-t)/(2*t);
                return `M ${startX},${startY} Q ${controlX},${controlY} ${endX},${endY}`;
             }
             return `M ${from.x-x},${from.y-y} L ${to.x+x},${to.y+y}`;
          }
       });

    tgroup.append("text")
        .attr("dominant-baseline", "middle")
        .attr("text-anchor", "middle")
        .call(make_editable, "label")
        .merge(transition.select("text"))
        .text((d) =>  {if(d.visible) return d.label; })
        .attr("x", (d : any) => { 
          let from = graph.nodes.find((s) => s.id==d.from);
          let to = graph.nodes.find((s) => s.id==d.to);
           if(d.loop){ return from.x; }
           let x = to.x - from.x;
           return (d.offset==null) ? (from.x + 0.5*x) : d.offset.x;
        })
        .attr("y", (d : any) => { 
          let from = graph.nodes.find((s) => s.id==d.from);
          let to = graph.nodes.find((s) => s.id==d.to);
           if(d.loop){ return from.y-140; }
           let y = to.y - from.y;
           return (d.offset==null) ? (from.y + 0.5*y - 20) : (d.offset.y-30);
        });

    transition.exit().remove();

   clearTimeout();
   setTimeout(displaySummary, 200);
}

render();

function make_editable(selection, field) {
    selection.on("click", function (d) {
        if (d3.event.ctrlKey || d3.event.shiftKey) return;
        let text = d3.select(this);
        let group = d3.select(this.parentNode);
        let bb = this.getBBox();
        let w = 100;
        let h = 30;
        let fo = group.append("foreignObject")
            .attr("x", bb.x - (w-bb.width)/2)
            .attr("y", bb.y - (h-bb.height)/2)
            .attr("width", w)
            .attr("height", h);
        let input = fo.append("xhtml:form")
            .append("input")
            .style("width", w + "px")
            .style("height", h + "px")
            .attr("type", "text")
            .attr("value", d[field])
            .classed("label-input", function () { this.focus(); this.select(); return true});
        input.on("blur", function () { fo.remove(); });
        input.on("keypress", function () {
            let e = d3.event;
            if (e.keyCode == 13) {
               let new_text = changedLabel(input, e);
               graph.transitions.forEach((transition) => {
                  if(transition.from == d[field]) transition.from = new_text;
                  if(transition.to == d[field]) transition.to = new_text;
               });
               d[field] = new_text;
               text.text(function (d) { return d[field]; });
                fo.remove();
            }
        });
    });
}

function changedLabel(input, event): string {
   input.on("blur", null);

   if (typeof (event.cancelBubble) !== 'undefined') {
      event.cancelBubble = true;
   }
   if (event.stopPropagation) {
      event.stopPropagation();
   }

   event.preventDefault();

   return clearText(input.property("value"));
}


// node dragging
let dragging: boolean;
d3.select(window)
   .on("keydown", () => {
      if (d3.event.ctrlKey || d3.event.metaKey) {
         dragging = true;
         svg.selectAll(".state").call(nodeDrag);
         svg.selectAll(".state").on("mousedown", null);
      }
   })
   .on("keyup", () => {
      if(dragging) {
         dragging = false;
         svg.selectAll(".state").on('.drag', null);
         svg.selectAll(".state").on("mousedown", (state) => {
             mousedownNode = state;
         });
      }
   });

let nodeDrag = d3.drag()
   .subject(() => {
      for (var i = graph.nodes.length - 1, state, x, y; i >= 0; --i) {
         state = graph.nodes[i];
         x = state.x - d3.event.x;
         y = state.y - d3.event.y;
         if (x * x + y * y < r * r) return state;
      }
   })
   .on("start", () => {
      graph.nodes.splice(graph.nodes.indexOf(d3.event.subject), 1);
      graph.nodes.push(d3.event.subject);
      if(d3.event.ctrlKey){
         d3.event.subject.active = true;
      }
   })
   .on("drag", () => {
      d3.event.subject.x = d3.event.x;
      d3.event.subject.y = d3.event.y;
   })
   .on("end", () => {
      d3.event.subject.active = false;
   })
   .on("start.render drag.render end.render", render);

/**
 * Display the summary if no states are drawn. Otherwise hide it.
 */
function displaySummary(): void {
   d3.select('#summary').classed('hidden', !!graph.nodes.length);
}

/**
 * Clear text that is entered by the user from invalid characters.
 * If the result is invalid 'INVALID_NAME' will be returned instead.
 * 
 * @param text User input
 */
function clearText(text: string): string {
   text = text.replace(/[^A-Za-z0-9_]/g, "");
   if(!/^[A-Za-z]/.test(text)) {
      text = 'INVALID_NAME';
   }
   return text;
}

/**
 * Create a default name for new states.
 */
function defaultStateName(): string {
   let name = `state_${i}`;
   i++;
   return name;
}

/**
 * Create a default name for new transitions.
 */
function defaultTransitionName(): string {
   let name = `transition_${j}`;
   j++;
   return name;
}

/**
 * Load a graph into the gui on startup.
 * Will only apply if the user provided --file option with valid input.
 * 
 * @param {Schema} graph The graph to draw 
 */
function load_graph(loadedGraph: Schema): void {
   if(loadedGraph) {
      graph.name = loadedGraph['name'];
      graph.nodes = loadedGraph['nodes'];
      graph.transitions = loadedGraph['transitions'];
      render();
   }
   d3.select('#graph-name-field').attr('value', graph.name);
}

EEL.loadFile(load_graph);
