/**
 * Interface for eel object pusblished by eel.js
 */
interface Eel {

    /**
     * Generate code by providing a Schema based representation of a FSM.
     * 
     * @param {Schema} fsm Schema based json
     * @param {string} language programming language
     * @param {Options} opts Options
     */
    parse(fsm: Schema, language: string, opts: Options): [string, string];

    /**
     * Get the current tool version as a string.
     */
    get_version(): string;

    /**
     * Get the documentation of the tool as a html-string.
     */
    get_info(): string;

    /**
     * Get all available languages as string.
     * Can be accessed as:
     *      languages = obj['languages']
     */
    available_languages(): string;

    /**
     * Get a Schema as a string to load on startup.
     */
    load_file(): string;

    /**
     * Set a default code generation option (opt) to the given value.
     */
    set_option(opt: string, value: string | boolean | number): void;

    /**
     * Unset a default code generation option (opt).
     */
    unset_option(opt: string): void;

    /**
     * Unset all currently set default code generation options.
     */
    unset_all_options(): void;
}

declare let eel: Eel;

/**
 * Interface for Code generation options.
 */
interface Options {
    "skip-validation"?: boolean | string,
}

/**
 * Convenience Wrapper for 'eel' object.
 * Can be accessed via const EEL.
 */
class EelWrapper {

    private static LANG = 'languages';

    public async parse(fsm: Schema, language: string, opts: Options, callback: (code: string, error: string) => any) {
        let graph = JSON.stringify(fsm);
        let options = JSON.stringify(opts);

        // @ts-ignore
        let [code, error] = await eel.parse(graph, language, options)();
        
        callback(code, error);
    }
    
    public async getVersion(callback: (v: string) => any) {
        // @ts-ignore
        let version = await eel.get_version()();
        callback(version);
    }
    
    public async getInfo(callback: (info: string) => any) {
        // @ts-ignore
        let info = await eel.get_info()();
        callback(info);
    }

    public async availableLanguages(callback: (languages: string[]) => any) {
        // @ts-ignore
        let available = await eel.available_languages()();
        callback(JSON.parse(available)[EelWrapper.LANG]);
    }

    public async loadFile(callback: (json: Schema) => any) {
        // @ts-ignore
        let graph = await eel.load_file()();
        callback(JSON.parse(graph));
    }

    public setOption(option: string, value: any) {
        eel.set_option(option, value);

    }

    public unsetOption(option: string) {
        eel.unset_option(option);

    }

    public unsetAllOptions() {
        eel.unset_all_options();
    }

    public async availableDefaults(callback: (defaults: Defaults) => any) {
        // @ts-ignore
        let defaults = await eel.available_defaults()();
        callback(JSON.parse(defaults));
    }
}

const EEL = new EelWrapper();
