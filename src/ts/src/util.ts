/**
 * 
 */
interface NodeOptions {
    content?: HTMLElement[] | string,
    type?: string,
    value?: string,
    src?: string,
    id?: string,
    classList?: string,
    disabled?: boolean
}

abstract class Creator {

    protected create(tagName: string, options?: NodeOptions): HTMLElement {
        let element = document.createElement(tagName);

        if(options) {
            let content = options['content'];
            if(content) {
                if(typeof(content) === 'string') {
                    element.innerHTML = content;
                } else {
                    for (const node of content) {
                        element.appendChild(node);
                    }
                }
            }

            let src = options['src'];
            if(src) {
                element.setAttribute('src', src);
            }

            let type = options['type'];
            if(type) {
                element.setAttribute("type", type);
            }

            let value = options['value'];
            if(value) {
                element.setAttribute("value", value);
            }

            let id = options['id'];
            if(id) {
                element.setAttribute('id', id);
            }

            let classList = options['classList'];
            if(classList) {
                element.setAttribute('class', classList);
            }

            if(options['disabled']) {
                element.setAttribute("disabled", "disabled");
            }
        }

        return element;
    }
}

/**
 * Wrapper class for a DOM HTMLElement.
 */
abstract class ElementWrapper extends Creator {

    constructor(public element?: HTMLElement) {
        super();
    }

    public appendSelf(container: HTMLElement): void {
        container.appendChild(this.element);
    }
    
}

/**
 * Class providing simple selection functionality thoughout the project.
 */
class SelectionHandler extends ElementWrapper {

    private static SELECTED = "selected";

    private mappedElements: Map<HTMLElement, HTMLElement[]>;
    private creator: (str: string) => HTMLElement;
    private selected: HTMLElement;

    constructor(element?: HTMLElement, creator?: (str: string) => HTMLElement) {
        super(element);
        this.mappedElements = new Map();
        this.creator = creator;
        this.selected = undefined;
    }

    public createOption(id: string, cascadeSelection?: HTMLElement[]): void {
        if(this.creator) {
            let option = this.creator(id);
            this.addOption(option, cascadeSelection);
            this.element.appendChild(option);
        }
    }

    public addOption(option: HTMLElement, cascadeSelection?: HTMLElement[]): void {
        this.mappedElements.set(option, cascadeSelection);
        option.addEventListener("click", () => this.select(option), false);
    }

    public select(selection: HTMLElement): void {
        let currentSelection = this.selected;

        let isSelect = selection !== currentSelection;

        if(currentSelection) {
            let cascadeSelection = this.mappedElements.get(currentSelection);
            this.selectInternal(currentSelection, false, cascadeSelection);
            this.selected = undefined;
        }
        
        if(isSelect) {
            let cascadeSelection = this.mappedElements.get(selection);
            this.selectInternal(selection, true, cascadeSelection);
            this.selected = selection;
        }
    }

    private selectInternal(element: HTMLElement, select: boolean, cascadeSelection?: HTMLElement[]) {
        if(select) {
            element.classList.add(SelectionHandler.SELECTED);
            if(cascadeSelection) {
                for (const furtherElement of cascadeSelection) {
                    furtherElement.classList.add(SelectionHandler.SELECTED);
                }
            }
        } else {
            element.classList.remove(SelectionHandler.SELECTED);
            if(cascadeSelection) {
                for (const furtherElement of cascadeSelection) {
                    furtherElement.classList.remove(SelectionHandler.SELECTED);
                }
            }
        }
    }
}
