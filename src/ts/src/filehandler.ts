// https://stackoverflow.com/a/30832210
// Function to download data to a file
function download(data, filename, type) {
    let file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        let a = document.createElement("a");
        let url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}

function graphCSSRules() {
    let style = "@import url('https://fonts.googleapis.com/css?family=Oswald:200'); * {font-family: 'Oswald', Courier, monospace;} ";

    for(let x = 0; x != document.styleSheets.length; x++) {
        let sheet : any = document.styleSheets[x];
        for(let y = 0; y != sheet.cssRules.length; y++) {
            let rule : any = sheet.cssRules[y];
            if(/svg|\.state|\.transition/g.test(rule.selectorText)) {
                let css: string = rule.cssText;
                let cutoff: string = '#graph-container';
                style += css.slice(css.lastIndexOf(cutoff) + cutoff.length);
            }
        }
    }

    return style;
}

function saveSVG() {
    if(graph.nodes.length !== 0) {
        download(svgToString(), `${filename()}.svg`, "image/svg+xml");
    } else {
        notifyOfEmptyGraph();
    }
}

function svgToString() {
    let svg : any = document.querySelector("svg").cloneNode(true);
    let styles = graphCSSRules();
    return `<?xml version="1.0" encoding="UTF-8"?>
        <svg 
            xmlns="http://www.w3.org/2000/svg" 
            xmlns:cc="http://creativecommons.org/ns#" 
            xmlns:dc="http://purl.org/dc/elements/1.1/" 
            xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" 
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
            xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" 
            xmlns:svg="http://www.w3.org/2000/svg" 
            height="210mm" 
            width="297mm" 
            viewBox="0 0 297 210" 
            version="1.1" 
            id="svg57" 
            >
            <defs id="defs51" />
            <sodipodi:namedview 
                id="base" 
                pagecolor="#ffffff" 
                bordercolor="#666666" 
                borderopacity="1.0" 
                inkscape:pageopacity="0.0" 
                inkscape:pageshadow="2" 
                inkscape:document-units="mm" 
                inkscape:current-layer="layer1" 
                showgrid="false" 
                inkscape:window-width="2560" 
                inkscape:window-height="1330" 
                inkscape:window-maximized="1" />
            <style type="text/css">${styles}</style>
            <metadata id="metadata54">
                <rdf:RDF>
                    <cc:Work rdf:about="">
                        <dc:format>image/svg+xml</dc:format>
                        <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                        <dc:title />
                    </cc:Work>
                </rdf:RDF>
            </metadata>
            <g transform="scale(0.25)" inkscape:label="Ebene 1" inkscape:groupmode="layer" id="layer1">${svg.innerHTML}</g>
        </svg>
    `;
}

let exchangeTypes = new Map([
    ["python", "py"],
    ["javascript", "js"],
    ["typescript", "ts"]
]);
function saveCode(code, language) {
    if(code && code.indexOf('ERROR') != 0) {
        let ending = language;
        if (exchangeTypes.has(language)) {
            ending = exchangeTypes.get(language);
        }
        download(code, `${filename()}.${ending}`, "text/plain");
    } else {
        window.alert('There is no code to save!');
    }
}

function savePNG(): void {
    if(graph.nodes.length > 0) {

        //code adoption of https://stackoverflow.com/questions/3975499/convert-svg-to-image-jpeg-png-etc-in-the-browser
    
        let downloadPNG = (data, filename) => {
            var evt = new MouseEvent("click", {
                view: window,
                bubbles: false,
                cancelable: true
            });
            var a = document.createElement("a");
            a.setAttribute("download", filename);
            a.setAttribute("href", data);
            a.setAttribute("target", '_blank');
            a.dispatchEvent(evt);
        }
         
        let canvas = document.createElement("canvas");
        let container = document.getElementById('graph-container');
        canvas.width = container.offsetWidth;
        canvas.height = container.offsetHeight;
        var context = canvas.getContext("2d");
        context.clearRect(0, 0, container.offsetWidth, container.offsetHeight);
        let DOMURL = window.URL || window.webkitURL || window;
        let img = new Image();
        let svgBlob = new Blob([svgToString()], {type: "image/svg+xml;charset=utf-8"});
        //@ts-ignore
        let url = DOMURL.createObjectURL(svgBlob);
        img.onload = () => {
            context.drawImage(img, 0, 0);
            //@ts-ignore
            DOMURL.revokeObjectURL(url);
            let imgURI = canvas
                .toDataURL("image/png")
                .replace("image/png", "image/octet-stream");
            downloadPNG(imgURI, `${filename()}.png`);
        };
        img.src = url;
    } else {
        notifyOfEmptyGraph();
    }
}

function saveJson(): void {
    if(graph.nodes.length > 0) {
        download(JSON.stringify(graph), `${filename()}.json`, 'text/json');
    } else {
        notifyOfEmptyGraph();
    }
}

function notifyOfEmptyGraph() {
    window.alert('Please draw a graph first before you save it!');
}

function filename(): string {
    return graph.name || 'graph';
}