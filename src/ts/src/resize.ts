class ResizeHandler extends ElementWrapper {

    private static DRAGGABLE_BORDER_SIZE: number = 8;

    private position: number;

    constructor(element: HTMLElement) {
        super(element);

        let listener = (mousemove) => this.resize(mousemove);
        this.element.addEventListener("mousedown", (mousedown) =>  {
            let undraggableSpace = parseInt(getComputedStyle(this.element, '').width) - ResizeHandler.DRAGGABLE_BORDER_SIZE - 2;
            if (mousedown.offsetX > undraggableSpace) {
                this.position = mousedown.x;
                document.addEventListener("mousemove", listener, false);
            }
        }, false);

        document.addEventListener("mouseup", () => {
            document.removeEventListener("mousemove", listener, false);
        }, false);
    }

    public resize(mousemove) {
        let distance = this.position - mousemove.x;
        this.position = mousemove.x;
        
        let newWidth = parseInt(getComputedStyle(this.element, '').width) - distance;

        this.element.style.width = `${newWidth}px`;
    }
}

const CODERESIZER = new ResizeHandler(document.getElementById('text-area'));
