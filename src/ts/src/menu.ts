/// <reference path="./util.ts"/>

/**
 * Wrapper class for the gui menu.
 */
class Menu extends ElementWrapper {

    private selection: SelectionHandler;

    constructor(element: HTMLElement) {
        super(element);
        this.selection = new SelectionHandler(this.element);
    }

    public init(): void {
        this.putMenuOption('help-btn', 'help');
        this.putMenuOption('info-btn', 'info');
        this.putMenuOption('languages-btn', 'languages');
        this.putMenuOption('options-btn', 'options');
    }

    private putMenuOption(buttonId: string, sectionId: string): void {
        let section = document.getElementById(sectionId);
        let button = document.getElementById(buttonId);
        this.selection.addOption(button, [this.element, section]);
    }
}

const MENU = new Menu(document.getElementById('menu'));
MENU.init();
