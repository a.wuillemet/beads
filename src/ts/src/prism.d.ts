/**
 * Declarative interface for prism.js - code highlighting.
 */

declare const Prism: PRISM;

declare interface PRISM {

    highlightAllUnder(element: HTMLElement): void;

}
