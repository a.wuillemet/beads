interface Schema {
    name : string,
    nodes : {
        id : string,
        x? : number,
        y? : number,
        start?: boolean
    }[],
    transitions : {
        label: string,
        from: string,
        to: string,
        visible: boolean,
        offset: object
    }[]
}
