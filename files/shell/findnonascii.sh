#!/bin/sh
#show textfiles containing unicode characters. Can cause problems.
grep --color='auto' -r -I -P -n "[\x80-\xFF]"
